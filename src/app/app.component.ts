import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { UserJson } from './json/user.json';
import { interval } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Lista de Usuários';

  private intervalTimmer: any;
  private biometricSubscription: any;

  users: Array<UserJson>;
  user: UserJson;

  constructor(
    private userService: UserService,
  ) {
    this.user = new UserJson();
    this.users = [];
    this.intervalTimmer = interval(500);
  }

  ngOnInit(): void {
    this.loadUsers();
  }

  loadUsers(): void {
    this.users = [];
    this.userService.getAll()
      .subscribe(
                  response => {
                    this.users = response;
                    if (this.users.length === 0) {
                      alert('Nenhum usuário cadastrado.');
                    }
                  },
                  error => {
                    console.log(error);
                    alert('Ops! Ocorreu um erro ao executar sua requisição!');
                  },
                );
  }

  addDigital(user: UserJson) {
    this.user = user;

    this.userService.addDigital(user)
      .subscribe(
                  response => {
                    this.startStatusListerner();

                  },
                  error => {
                    console.log(error);
                    alert('Ops! Ocorreu um erro ao executar sua requisição!');
                  },
                );


  }


  getStatusAddDigital() {
    this.userService.getAddDigitalStatus()
      .subscribe(
                  response => {
                    this.user.biometricInfos = response.message;
                    if (response.biometricStatus === 'REGISTRATION_SUCCESSFUL') {
                      this.stopStatusListerner();
                    }
                    console.log(response.message);
                  },
                  error => {
                    console.log(error);
                    alert('Ops! Ocorreu um erro ao executar sua requisição!');
                  },
                );
  }

  saveNewUser() {
    this.userService.save(this.user)
      .subscribe(
                  response => {
                    this.loadUsers();
                    this.user = new UserJson();
                  },
                  error => {
                    console.log(error);
                    alert('Ops! Ocorreu um erro ao executar sua requisição!');
                  },
                );
  }

  private startStatusListerner() {

    this.biometricSubscription = this.intervalTimmer.subscribe(
      (value: number) => {
        this.getStatusAddDigital();
      },
      (error: any) => {
        console.error('error');
      },
      () => {
        console.log('observable completed !');
      }
    );
  }

  private stopStatusListerner() {
    this.biometricSubscription.unsubscribe();
  }

}
