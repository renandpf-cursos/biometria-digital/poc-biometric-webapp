import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserJson } from './json/user.json';
import { environment } from '../environments/environment';
import { BiometricResponseJson } from './json/biometric-response.json';

export class UserService {

  constructor(private http: HttpClient) {}


  getAll(): Observable<Array<UserJson>> {
    const url = environment.endpoint + '/user';
    return this
      .http
      .get<Array<UserJson>>(
        url
      );
  }

  save(user: UserJson): Observable<void> {
    const url = environment.endpoint + '/user';
    return this
      .http
      .post<void>(
        url, user
      );
  }

  addDigital(user: UserJson): Observable<void> {
    const url = environment.endpoint + '/biometric/request-enroll-user';
    return this
      .http
      .post<void>(
        url, user
      );
  }

  getAddDigitalStatus(): Observable<BiometricResponseJson> {
    const url = environment.endpoint + '/biometric/request-enroll-user-status';
    return this
      .http
      .get<BiometricResponseJson>(
        url
      );
  }
}
