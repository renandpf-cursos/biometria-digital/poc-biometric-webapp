export class BiometricResponseJson {
    message: string;
    biometricStatus: string;
    errorCode: string;
    errorMessage: string;
}
